// import {Fragment} from 'react'
import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import CourseView from './pages/CourseView'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
//if version 5 and below = Switch, if version 6 and up = Routes
//import Banner from './components/Banner'
//import Highlights from './components/Highlights'
import Courses from './pages/Courses'

import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/ErrorPage'

import './App.css';
import { UserProvider} from './UserContext'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null

  })

  const unsetUser = () => {
    localStorage.clear()

  }



useEffect(() => {
  let token = localStorage.getItem('token');
  fetch('http://localhost:4000/users/details', {
    method: "GET",
    headers: {
      Authorization : `Bearer ${token}`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)

    if(typeof data._id !== "undefined"){
      setUser({
        id:data._id,
        isAdmin: data.isAdmin
      })
    } else {
      setUser({
        id: null,
        isAdmin: null
      })
    }
  })
    }, [user])



  // console.log(user)
  // console.log(localStorage)



//if version 5 and below = component, if version 6 and up = element and with self closing tag <Route path ="/" element ={<Home/>}/>}

  return (
    <UserProvider value ={{user, setUser, unsetUser}} >
    <Router>
      <AppNavbar />
      <Container>
      <Switch>
        <Route exact path="/" component ={Home} />
        <Route exact path="/courses" component={Courses} />
        <Route exact path="/courses/:courseId" component={CourseView}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/logout" component={Logout}/>
        <Route path="*" component={NotFound} />
      </Switch>
      </Container>

    </Router>
    </UserProvider>
  );
}

export default App;

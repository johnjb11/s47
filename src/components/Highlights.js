import {Row, Col, Card} from 'react-bootstrap'
/* start always in new component file
export default function Highlights (){
	return(


		)
}
*/


export default function Highlights (){
	return(
	<Row className = "mt-3 mb-3">
		<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Learn from Home</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci, itaque. Ipsam eveniet recusandae nihil officia totam in necessitatibus eaque, alias exercitationem, quisquam, voluptas unde, reiciendis culpa dignissimos impedit qui quae.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs = {12} md = {4}>
			<Card>
				<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci, itaque. Ipsam eveniet recusandae nihil officia totam in necessitatibus eaque, alias exercitationem, quisquam, voluptas unde, reiciendis culpa dignissimos impedit qui quae.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs = {12} md = {4}>
			<Card>
				<Card.Body>
					<Card.Title>
						<h2>Be part of our community</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci, itaque. Ipsam eveniet recusandae nihil officia totam in necessitatibus eaque, alias exercitationem, quisquam, voluptas unde, reiciendis culpa dignissimos impedit qui quae.
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	</Row>

		)
}
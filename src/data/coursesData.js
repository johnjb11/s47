const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Quasi voluptate dolor ullam, voluptas nulla! Impedit tempore molestias tenetur ratione consequuntur laboriosam temporibus architecto, maxime, deserunt obcaecati eum aperiam provident",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Quasi voluptate dolor ullam, voluptas nulla! Impedit tempore molestias tenetur ratione consequuntur laboriosam temporibus architecto, maxime, deserunt obcaecati eum aperiam provident",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java- Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Quasi voluptate dolor ullam, voluptas nulla! Impedit tempore molestias tenetur ratione consequuntur laboriosam temporibus architecto, maxime, deserunt obcaecati eum aperiam provident",
		price: 60000,
		onOffer: true
	}
]

export default coursesData;